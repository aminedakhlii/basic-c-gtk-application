/*
 * Copyright (c) 2019 Amine Dakhli <amine.dakhli@medtech.tn>
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>



void second_window(GtkWidget *button, gpointer window)
{
    GtkWidget *window2 , * Qbtn , *label2 , *grid2 , *fixed2 ;


    window2 = gtk_window_new(GTK_WINDOW_TOPLEVEL) ;
    gtk_window_set_title(GTK_WINDOW(window2), "window 2");
    gtk_window_set_default_size(GTK_WINDOW(window2), 600 , 500);
    gtk_window_set_position(GTK_WINDOW(window2), GTK_WIN_POS_CENTER);

    fixed2 = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(window2), fixed2);


    label2 = gtk_label_new("second window");
    gtk_fixed_put(GTK_FIXED(fixed2), label2, 250, 20);
    gtk_widget_set_size_request(label2 , 80 , 30);


    Qbtn = gtk_button_new_with_label("Quit");
    g_signal_connect(Qbtn , "clicked", G_CALLBACK(gtk_main_quit), NULL);
    gtk_fixed_put(GTK_FIXED(fixed2), Qbtn, 276 , 190);

    g_signal_connect(G_OBJECT(window2), "destroy",
          G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_show_all(window2);


}

void create_window(GtkWidget *button, gpointer window) {

GtkWidget *win, *label;


    label = gtk_label_new("Username and Password is incorrect.");
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_container_add(GTK_CONTAINER(win), label);
    gtk_widget_show_all(win);
}


static void destroy(GtkWidget *widget, gpointer data){
gtk_main_quit();
}

int main(int argc, char *argv[]){

 GtkWidget *window;
 GtkWidget *grid;
 GtkWidget *Login_button, *Quit_button;
 GtkWidget *u_name;
 GtkWidget *pass;
 GtkWidget *label_user;
 GtkWidget *label_pass ;
 GtkWidget  *button_container;

 gtk_init(&argc, &argv);

 window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
 gtk_window_set_title(GTK_WINDOW(window), "Login page");
 gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
 gtk_container_set_border_width(GTK_CONTAINER(window), 300);
 gtk_window_set_resizable(GTK_WINDOW(window), FALSE);

 grid = gtk_grid_new();
 gtk_grid_set_row_spacing(GTK_GRID(grid), 3);
 gtk_container_add(GTK_CONTAINER(window), grid);

 label_user = gtk_label_new("Username  ");
 label_pass = gtk_label_new("Password  ");


 u_name = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(u_name), "Username");
 gtk_grid_attach(GTK_GRID(grid), label_user, 0, 1, 1, 1);
 gtk_grid_attach(GTK_GRID(grid), u_name, 1, 1, 2, 1);

 pass = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(pass), "Password");
 gtk_grid_attach(GTK_GRID(grid), label_pass, 0, 2, 1, 1);
 gtk_entry_set_visibility(GTK_ENTRY(pass), 0);
 gtk_grid_attach(GTK_GRID(grid), pass, 1, 2, 1, 1);

 Login_button = gtk_button_new_with_label("Log in");
 g_signal_connect(Login_button, "clicked", G_CALLBACK(second_window), NULL);
 gtk_grid_attach(GTK_GRID(grid), Login_button, 0, 3, 2, 1);

 Quit_button = gtk_button_new_with_label("Quit");
 g_signal_connect(Quit_button, "clicked", G_CALLBACK(gtk_main_quit), NULL);
 gtk_grid_attach(GTK_GRID(grid), Quit_button, 0, 4, 2, 1);


 gtk_widget_show_all(window);
 gtk_main();

return 0;
}
