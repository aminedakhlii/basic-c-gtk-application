/*
 * Copyright (c) 2019 Amine Dakhli <amine.dakhli@medtech.tn>
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include "share.h"
#include <curl/curl.h>
#include "curl.h"

struct user_info
{
   GtkWidget * u_name ;
   GtkWidget * pass ;
   GtkWidget * roomnum ;
} ;

struct new_user_info
{
   GtkWidget * new_name ;
   GtkWidget * new_lastname ;
   GtkWidget * new_username ;
   GtkWidget * new_pass ;
   GtkWidget * new_mail ;
   GtkWidget * new_pass_confirmation ;
   GtkWidget * roomn ;

} ;


void sharing_screen_window(GtkWidget *button, gpointer data )
{

    GtkWidget *window2 , *btn , *label2  , *fixed2 ;

    struct user_info *d = data ;

    const char * username ;
    username = (char *)malloc(20) ;
    username = gtk_entry_get_text(GTK_ENTRY(d->u_name));

    const char * password ;
    password = (char *)malloc(20) ;
    password = gtk_entry_get_text(GTK_ENTRY(d->pass)) ;

    const char * roomnum ;
    roomnum = (char *)malloc(20) ;
    roomnum = gtk_entry_get_text(GTK_ENTRY(d->roomnum));

    char * ip ;
    ip= (char*)malloc(40) ;


    int ret = curl_function(username , password, roomnum, ip);

    if(ret == 0)
    {
    window2 = gtk_window_new(GTK_WINDOW_TOPLEVEL) ;
    gtk_window_set_title(GTK_WINDOW(window2), "Sharing screen page");
    gtk_window_set_default_size(GTK_WINDOW(window2), 600 , 500);
    gtk_window_set_position(GTK_WINDOW(window2), GTK_WIN_POS_CENTER);

    fixed2 = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(window2), fixed2);


    label2 = gtk_label_new("start screen sharing");
    gtk_fixed_put(GTK_FIXED(fixed2), label2, 235, 20);
    gtk_widget_set_size_request(label2 , 80 , 30);


    btn = gtk_button_new_with_label("Start Sharing");
    g_signal_connect(btn , "clicked", G_CALLBACK(share), ip);
    gtk_fixed_put(GTK_FIXED(fixed2), btn , 250 , 150);
    gtk_widget_set_size_request(btn , 100 , 30);


    char no[1] ="0" ;

    btn = gtk_button_new_with_label("STOP");
    g_signal_connect(btn , "clicked", G_CALLBACK(share), no);
    gtk_fixed_put(GTK_FIXED(fixed2), btn , 250 , 190);
    gtk_widget_set_size_request(btn , 100 , 30);


    GtkWidget * Qbtn = gtk_button_new_with_label("Quit");
    g_signal_connect(Qbtn , "clicked", G_CALLBACK(gtk_main_quit), NULL);
    gtk_fixed_put(GTK_FIXED(fixed2), Qbtn, 276 , 240);

    g_signal_connect(G_OBJECT(window2), "destroy",
          G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_show_all(window2);
    }
    else if(ret == -1 )
    {

    GtkWidget *window3 , *label3 ;


        window3 = gtk_window_new(GTK_WINDOW_TOPLEVEL) ;
        gtk_window_set_title(GTK_WINDOW(window3), "ERROR GETTING ROOM IP");
        gtk_window_set_default_size(GTK_WINDOW(window3), 300 , 200);
        gtk_window_set_position(GTK_WINDOW(window3), GTK_WIN_POS_CENTER);
        label3 =gtk_label_new("Error while getting the room ip address :\n invalid room name");
        GtkWidget * fixed3 = gtk_fixed_new();
        gtk_container_add(GTK_CONTAINER(window3), fixed3);
        gtk_fixed_put(GTK_FIXED(fixed3), label3, 30 , 15 );
        gtk_widget_show_all(window3);
    }
    else if(ret == -2 )
    {

    GtkWidget *window4 , *label4 ;


        window4 = gtk_window_new(GTK_WINDOW_TOPLEVEL) ;
        gtk_window_set_title(GTK_WINDOW(window4), "ERROR LOGIN FAILED");
        gtk_window_set_default_size(GTK_WINDOW(window4), 300 , 50);
        gtk_window_set_position(GTK_WINDOW(window4), GTK_WIN_POS_CENTER);
        label4 =gtk_label_new("Error while logging in") ;
        GtkWidget * fixed4 = gtk_fixed_new();
        gtk_container_add(GTK_CONTAINER(window4), fixed4);
        gtk_fixed_put(GTK_FIXED(fixed4), label4, 87 , 15 );

        gtk_widget_show_all(window4);
    }
printf("%s\n",ip);

}


void new_user_window(GtkWidget * button  , gpointer data2)
{
    GtkWidget *window5 , *label5 ;

    struct new_user_info *s = data2 ;


    const char * new_user_username ;
    new_user_username = (char *)malloc(20) ;
    new_user_username = gtk_entry_get_text(GTK_ENTRY(s->new_username)) ;


    const  char * new_user_lastname ;
    new_user_lastname = (char *)malloc(20) ;
    new_user_lastname = gtk_entry_get_text(GTK_ENTRY(s->new_lastname)) ;

    const char * new_user_firstname ;
    new_user_firstname = (char *)malloc(20) ;
    new_user_firstname = gtk_entry_get_text(GTK_ENTRY(s->new_name));

    const char * new_user_mail ;
    new_user_mail = (char *)malloc(50) ;
    new_user_mail = gtk_entry_get_text(GTK_ENTRY(s->new_mail));

    const char * new_user_pass ;
    new_user_pass = (char *)malloc(20) ;
    new_user_pass = gtk_entry_get_text(GTK_ENTRY(s->new_pass));

    const char * new_user_pc ;
    new_user_pc = (char*)malloc(20) ;
    new_user_pc = gtk_entry_get_text(GTK_ENTRY(s->new_pass_confirmation));




    if (strcmp(gtk_entry_get_text(GTK_ENTRY(s->new_pass)),gtk_entry_get_text(GTK_ENTRY(s->new_pass_confirmation))) == 0 && strcmp(gtk_entry_get_text(GTK_ENTRY(s->new_pass_confirmation)),"") != 0)
    {
        send_user_info(new_user_username , new_user_firstname ,new_user_lastname , new_user_pass) ;

        GtkWidget * window4 = gtk_window_new(GTK_WINDOW_TOPLEVEL) ;
        gtk_window_set_title(GTK_WINDOW(window4), "ERROR LOGIN FAILED");
        gtk_window_set_default_size(GTK_WINDOW(window4), 300 , 50);
        gtk_window_set_position(GTK_WINDOW(window4), GTK_WIN_POS_CENTER);
        GtkWidget* label4 =gtk_label_new("Welcome to Easyshow") ;
        GtkWidget * fixed4 = gtk_fixed_new();
        gtk_container_add(GTK_CONTAINER(window4), fixed4);
        gtk_fixed_put(GTK_FIXED(fixed4), label4, 87 , 15 );
        //GtkWidget * signin = gtk_button_new_with_label("Sign in");
       // g_signal_connect(signin, "clicked", G_CALLBACK(sharing_screen_window),  d);
       // gtk_fixed_put(GTK_FIXED(fixed4), signin, 87 , 25);
       // gtk_widget_set_size_request(signin , 190 , 40);

        gtk_widget_show_all(window4);
    }
    else
    {



        window5 = gtk_window_new(GTK_WINDOW_TOPLEVEL) ;
        gtk_window_set_title(GTK_WINDOW(window5), "ERROR LOGIN FAILED");
        gtk_window_set_default_size(GTK_WINDOW(window5), 300 , 50);
        gtk_window_set_position(GTK_WINDOW(window5), GTK_WIN_POS_CENTER);
        label5 =gtk_label_new("Password confirmation Failed") ;
        GtkWidget * fixed5 = gtk_fixed_new();
        gtk_container_add(GTK_CONTAINER(window5), fixed5);
        gtk_fixed_put(GTK_FIXED(fixed5), label5, 87 , 15 );

        gtk_widget_show_all(window5);
    }


}


int main(int argc, char *argv[]){

 GtkWidget *window;
 GtkWidget *grid , *fixed;
 GtkWidget *Login_button, *Quit_button , *signin_button;
 GtkWidget *label_user;
 GtkWidget *label_student , * label_signin;
 GtkWidget  * button_container;
 GtkWidget * image ;
 GdkColor color;

 struct user_info  user ;
 struct new_user_info new_user ;

 gtk_init(&argc, &argv);

 window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
 gtk_window_set_title(GTK_WINDOW(window), "Login page");
 gtk_window_set_default_size(GTK_WINDOW(window), 900 , 650);
 gtk_window_set_resizable(window , FALSE);
 gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);


 gdk_color_parse("#FFFffa", &color);


 fixed = gtk_fixed_new();
 gtk_container_add(GTK_CONTAINER(window), fixed);

 image = gtk_image_new_from_file("barre.jpeg");
 gtk_widget_modify_bg(GTK_WIDGET(window), GTK_STATE_NORMAL, &color);


 label_user = gtk_label_new("Login as teacher");
 label_student = gtk_label_new("Share the screen as Student");
 label_signin =gtk_label_new("Sign in");
 gtk_fixed_put(GTK_FIXED(fixed), label_signin, 620 , 150 );
 gtk_widget_modify_font (label_signin, pango_font_description_from_string ("Monospace 20"));

 gtk_fixed_put(GTK_FIXED(fixed), label_student, 100 , 150 );
 gtk_fixed_put(GTK_FIXED(fixed), label_user, 310 , 25 );




 user.u_name = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(user.u_name), "Username");
 gtk_fixed_put(GTK_FIXED(fixed), user.u_name, 420 , 20);

 user.pass = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(user.pass), "Password");
 gtk_entry_set_visibility(GTK_ENTRY(user.pass), 0);
 gtk_fixed_put(GTK_FIXED(fixed), user.pass, 600 , 20);


 user.roomnum = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(user.roomnum), "classroom");
 gtk_fixed_put(GTK_FIXED(fixed), user.roomnum, 30 , 20);


 new_user.new_name = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(new_user.new_name), "Name");
 gtk_fixed_put(GTK_FIXED(fixed), new_user.new_name, 510 , 200);

 new_user.new_lastname = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(new_user.new_lastname), "Last name");
 gtk_fixed_put(GTK_FIXED(fixed), new_user.new_lastname, 690 , 200);

 new_user.new_mail = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(new_user.new_mail), "E-mail address");
 gtk_fixed_put(GTK_FIXED(fixed), new_user.new_mail, 510 , 250);

 new_user.new_username = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(new_user.new_username), "Username");
 gtk_fixed_put(GTK_FIXED(fixed), new_user.new_username, 690 , 250);


 new_user.new_pass = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(new_user.new_pass), "Password");
 gtk_entry_set_visibility(GTK_ENTRY(new_user.new_pass), 0);
 gtk_fixed_put(GTK_FIXED(fixed), new_user.new_pass, 510 , 300);

 new_user.new_pass_confirmation = gtk_entry_new();
 gtk_entry_set_placeholder_text(GTK_ENTRY(new_user.new_pass_confirmation), "Confirm password");
 gtk_entry_set_visibility(GTK_ENTRY(new_user.new_pass_confirmation), 0);
 gtk_fixed_put(GTK_FIXED(fixed), new_user.new_pass_confirmation, 690 , 300);


 Login_button = gtk_button_new_with_label("Log in");
 g_signal_connect(Login_button, "clicked", G_CALLBACK(sharing_screen_window), &user );
 gtk_fixed_put(GTK_FIXED(fixed), Login_button, 780 , 20);
 gtk_widget_set_size_request(Login_button , 100 , 30);

 signin_button = gtk_button_new_with_label("Sign in");
 g_signal_connect(signin_button, "clicked", G_CALLBACK(new_user_window), &new_user);
 gtk_fixed_put(GTK_FIXED(fixed), signin_button, 600 , 370);
 gtk_widget_set_size_request(signin_button , 190 , 40);


 Quit_button = gtk_button_new_with_label("Quit");
 g_signal_connect(Quit_button, "clicked", G_CALLBACK(gtk_main_quit), NULL);
 gtk_fixed_put(GTK_FIXED(fixed), Quit_button, 790 , 600);
 gtk_widget_set_size_request(Quit_button , 100 , 30);

 g_signal_connect (window, "destroy",G_CALLBACK (gtk_main_quit), NULL);

 gtk_widget_show_all(window);
 gtk_main();


return 0;
}
