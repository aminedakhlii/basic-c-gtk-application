/*
 * Copyright (c) 2019 Amine Ben Hassouna <amine.benhassouna@gmail.com>
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sqlite3.h>

typedef struct classes
{
    char roomnumber[7] ;
    char ip[20] ;
} Room ;

 typedef struct users
{
    char username[20] ;
    char password[20] ;
} User ;

typedef struct full_user_info
{
    char name[20];
    char lastname[20];
    char username[20];
    char passwd[20];

} Uinfo ;

bool create_info_table(sqlite3 * db)
{
    sqlite3_stmt* statement;
    int rc;
    const char* sql_create_info_table = "CREATE TABLE IF NOT EXISTS users_full_info ("
                                        "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                                        "name TEXT NOT NULL,"
                                        "lastname TEXT,"
                                        "username TEXT,"
                                        "password);";

    // Execute the previous query to create the 'user' table
    rc = sqlite3_prepare_v2(db, sql_create_info_table, strlen(sql_create_info_table), &statement, NULL);
    if(rc == SQLITE_OK) {
        rc = sqlite3_step(statement);
        sqlite3_finalize(statement);

    } else {
        printf("Can't initialize the database: %s\n", sqlite3_errmsg(db));
        return false;
    }
    sqlite3_finalize(statement);

    return true;
}

bool insert_nuser(sqlite3 * db , Uinfo * user)
{
    sqlite3_stmt* statement;
    int rc;

    // Prepare the SQL query to insert the new user
    char sql_insert_info[250];
    sprintf(sql_insert_info,
            "INSERT INTO users_full_info (name, lastname, username, password) VALUES ('%s', '%s', '%s', '%s')",
            user->name,
            user->lastname,
            user->username,
            user->passwd) ;

    rc = sqlite3_prepare_v2(db, sql_insert_info, strlen(sql_insert_info), &statement, NULL);
    if(rc == SQLITE_OK) {
        rc = sqlite3_step(statement);


    }

    sqlite3_finalize(statement);
    return true;
}

bool delete_room_data(sqlite3 * db , char room[7])
{
    sqlite3_stmt* statement;
    int rc;
    char sql_delete_room[250];
    sprintf(sql_delete_room,
            "DELETE FROM classes WHERE classroom = '%s'",
            room);
    rc = sqlite3_prepare_v2(db, sql_delete_room, strlen(sql_delete_room), &statement, NULL);
    if(rc == SQLITE_OK) {
        rc = sqlite3_step(statement);


    }
    else printf("%s",sqlite3_errmsg(db)) ;
    sqlite3_finalize(statement);
    return true;

}


bool insert_Room (sqlite3 * db , Room * room )
{
    sqlite3_stmt* statement;
    int rc;

    // Prepare the SQL query to insert the new user
    char sql_insert_room[250];
    sprintf(sql_insert_room,
            "INSERT INTO classes (classroom, ip) VALUES ('%s', '%s')",
            room->roomnumber,
            room->ip);

    // Execute the previous query to insert a new line into the 'user' table
    rc = sqlite3_prepare_v2(db, sql_insert_room, strlen(sql_insert_room), &statement, NULL);
    if(rc == SQLITE_OK) {
        rc = sqlite3_step(statement);


    }
    sqlite3_finalize(statement);
    return true;

}
bool verify_user (sqlite3* db , User user )
{
    sqlite3_stmt* statement;
    int rc;

    const char * sql_select_user ="SELECT username, password FROM user" ;

    rc = sqlite3_prepare_v2(db, sql_select_user, strlen(sql_select_user), &statement, NULL);

    if(rc == SQLITE_OK) {

        while(sqlite3_step(statement) == SQLITE_ROW) {

            const char * userdb = (const char *)sqlite3_column_text(statement, 0) ;
            const char * passdb = (const char *)sqlite3_column_text(statement, 1) ;

            if((!strcmp(userdb, user.username)) && ( !strcmp(passdb , user.password)) ) {
                    return true ;
                }

                }
            }
    sqlite3_finalize(statement);

    return false ;
        }


bool room_ip( sqlite3 * db , char room[7] , char * ipp )
{
    if (!strcmp (room,"")) return false ;
    sqlite3_stmt* statement ;
    int rc ;

    const char *sql_find_room_ip = "SELECT classroom , ip FROM classes" ;
    rc = sqlite3_prepare_v2(db, sql_find_room_ip, strlen(sql_find_room_ip), &statement, NULL);

    if (rc == SQLITE_OK) {

        while(sqlite3_step(statement) == SQLITE_ROW) {

            const char * roomdb = (const char *)sqlite3_column_text(statement, 0) ;
            const char * ipdb = (const char *)sqlite3_column_text(statement, 1) ;

            if((strcmp(roomdb, room) == 0)) {
                    strcpy(ipp,ipdb) ;
                    return true ;
                } }



    }
    sqlite3_finalize(statement);

    return false ;
}
bool create_user_table(sqlite3* db)
{
    sqlite3_stmt* statement;
    int rc;

    // SQL query to create the 'user' table
    const char* sql_create_user_table = "CREATE TABLE IF NOT EXISTS user ("
                                        "username TEXT NOT NULL,"
                                        "password TEXT);";

    // Execute the previous query to create the 'user' table
    rc = sqlite3_prepare_v2(db, sql_create_user_table, strlen(sql_create_user_table), &statement, NULL);
    if(rc == SQLITE_OK) {
        rc = sqlite3_step(statement);
        sqlite3_finalize(statement);

    } else {
        printf("Can't initialize the database: %s\n", sqlite3_errmsg(db));
        return false;
    }

    sqlite3_finalize(statement);

    return true;
}

bool create_room_table(sqlite3* db)
{
    sqlite3_stmt* statement;
    int rc;

    // SQL query to create the 'user' table
    const char* sql_create_user_table = "CREATE TABLE IF NOT EXISTS classes ("
                                        "classroom TEXT,"
                                        "ip TEXT);";

    // Execute the previous query to create the 'user' table
    rc = sqlite3_prepare_v2(db, sql_create_user_table, strlen(sql_create_user_table), &statement, NULL);
    if(rc == SQLITE_OK) {
        rc = sqlite3_step(statement);
        sqlite3_finalize(statement);

    } else {
        printf("Can't initialize the database: %s\n", sqlite3_errmsg(db));
        return false;
    }
    sqlite3_finalize(statement);

    return true;
}


bool insert_user(sqlite3* db, User* user)
{
    sqlite3_stmt* statement;
    int rc;

    // Prepare the SQL query to insert the new user
    char sql_insert_user[250];
    sprintf(sql_insert_user,
            "INSERT INTO user (username, password) VALUES ('%s', '%s')",
            user->username,
            user->password);

    // Execute the previous query to insert a new line into the 'user' table
    rc = sqlite3_prepare_v2(db, sql_insert_user, strlen(sql_insert_user), &statement, NULL);
    if(rc == SQLITE_OK) {
        rc = sqlite3_step(statement);
    }

    sqlite3_finalize(statement);
    return true;
}

int main (int argc , char * argv[])
{


    printf("content-type: text/html \n\n");

    User new_user, user ;
    sqlite3* db , *classes , *fulldb;
    Room  nroom ;

    int rc , rc2 , rc3 ;

    rc = sqlite3_open("/usr/lib/cgi-bin/users3.db", &db);
    if(rc != SQLITE_OK) {
        printf("Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }


    // If the 'user' table does not exists
        // Create 'user' table
        if(!create_user_table(db)) {
            sqlite3_close(db);
            return 1;
        }

        rc2 = sqlite3_open("/usr/lib/cgi-bin/classes.db", &classes);
        if(rc != SQLITE_OK) {
            printf("Can't open database: %s\n", sqlite3_errmsg(db));
            sqlite3_close(classes);
            return 1;
        }

        if(!create_room_table(classes)) {
            sqlite3_close(classes);
            return 1;
        }


    char login[20] ;
    char password[20] ;

    char oldlogin[20] ;
    char oldpwd [20] ;
    char room[7] ;

    char updateroom[7] ;
    char updateip[20] ;

// the classroom data sent from the datashow program



    char * roomdata = getenv("QUERY_STRING") ;

    sscanf(roomdata, "classroom=%[^&]&ip=%[^&]",updateroom,updateip) ;
    strcpy(nroom.roomnumber, updateroom ) ;
    strcpy(nroom.ip , updateip) ;
    delete_room_data(classes , updateroom);
    if(!insert_Room(classes, &nroom))
        printf("sOrry \n");




// The data sent from the login


    char * logindata ;
    logindata = getenv("QUERY_STRING") ;
    //char * len_ = getenv("CONTENT_LENGTH");
   // long int  len = strtol(len_, NULL, 10);
  //  logindata = malloc(len + 1);
    //fgets(logindata, len + 1 , stdin);
    sscanf(logindata,"login=%[^&]&pwd=%[^&]&room=%[^&]",oldlogin, oldpwd,room) ;
    strcpy(user.username, oldlogin ) ;
    strcpy(user.password , oldpwd) ;

    if(verify_user(db, user))
    {
        char * ip ;
        ip = (char *)malloc(20) ;
        if(room_ip(classes , room , ip ))
        {
            printf("%s",ip) ;
        }
        else printf("0.0.0.0") ;
    }
    else printf("0.0.0.1");

    // The email verification :


// if the email is verified send its data as query to the next process



    // the sign in process after verifing the teacher s mail

    char name[20] , lastn[20] , uname[20] , npwd[20] ;

    char *signdata = getenv("QUERY_STRING") ;

sscanf(signdata,"new_name=%[^&]&new_lastname=%[^&]&new_username=%[^&]&newpwd=%[^&]",
       name, lastn , uname , npwd);

Uinfo newsigned ;

strcpy(newsigned.name, name ) ;
strcpy(newsigned.lastname , lastn) ;
strcpy(newsigned.username, uname ) ;
strcpy(newsigned.passwd , npwd) ;

rc3 = sqlite3_open("/usr/lib/cgi-bin/full.db", &fulldb);
if(rc3 != SQLITE_OK) {
    printf("Can't open database: %s\n", sqlite3_errmsg(fulldb));
    sqlite3_close(fulldb);
    return 1;
}

if(!create_info_table(fulldb)) {
    sqlite3_close(fulldb);
    return 1;
}
if(!insert_nuser(fulldb, &newsigned))
    printf("sOrry \n");
else {
    strcpy(new_user.username, uname ) ;
    strcpy(new_user.password , npwd) ;
    if(!insert_user(db, &new_user))
        printf("sOrry \n");

}

    sqlite3_close(fulldb) ;
    sqlite3_close(classes) ;
    sqlite3_close(db);



return 0 ;
}
